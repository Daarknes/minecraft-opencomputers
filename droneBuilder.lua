local drone = component.proxy(component.list("drone")())
local inv = component.proxy(component.list("inventory_controller")())

local sides = {
	down = 0,
	up = 1,
	north = 2,
	south = 3,
	west = 4,
	east = 5
}

--[[ INFO:
all of this is orientation dependent:
height refers to the y-direction, width to x-direction and depth to z-direction.
--]]

local targetX, targetY, targetZ = -5, 0, 0
local inventorySide = sides.east

-- shortcuts
local wall = "compactmachines3:wallbreakable"
local rs = "minecraft:redstone"
local ep = "minecraft:ender_pearl"

local schematics = {
	compactMachineWall = {
		blocks = {
			{
				{"minecraft:iron_block"}
			},
			{
				{rs}
			}
		},
		drop = rs
	},
	tunnel = {
		blocks = {
			{
				{rs, rs, rs},
				{rs, wall, rs},
				{rs, rs, rs}
			},
			{
				{"", "", ""},
				{"", "minecraft:hopper", ""},
				{"", "", ""}
			}
		},
		drop = rs
	},
	tinyCompactMachine = {
		blocks = {
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, "", wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			}
		},
		drop = ep
	},
	smallCompactMachine = {
		blocks = {
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, "minecraft:iron_block", wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			}
		},
		drop = ep
	},
	normalCompactMachine = {
		blocks = {
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, "minecraft:gold_block", wall},
				{wall, wall, wall}
			},
			{
				{wall, wall, wall},
				{wall, wall, wall},
				{wall, wall, wall}
			}
		},
		drop = ep
	},
	largeCompactMachine = {
		blocks = {
			{
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall}
			},
			{
				{wall, wall, wall, wall, wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, wall, wall, wall, wall}
			},
			{
				{wall, wall, wall, wall, wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, wall, wall, wall, wall}
			},
			{
				{wall, wall, wall, wall, wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, "", "", "", wall},
				{wall, wall, wall, wall, wall}
			},
			{
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall},
				{wall, wall, wall, wall, wall}
			}
		},
		drop = ep
	}
}


----------------- drone helpers ----------------------------
local _x, _y, _z = 0, 0, 0

local function sleep(timeout)
    local deadline = computer.uptime() + (timeout or 0)
    repeat
        computer.pullSignal(deadline - computer.uptime())
    until computer.uptime() >= deadline
end

local function move(dx, dy, dz)
	_x = _x + dx
	_y = _y + dy
	_z = _z + dz

	drone.move(dx, dy, dz)
	sleep(math.sqrt(math.abs(dx) + math.abs(dy)*1.5 + math.abs(dz)) * 0.35)
end

local function home()
	move(-_x, -_y , -_z)
end


---------------------------------------------------------------------------

local function stringToStack(str, amount)
	local res = string.gmatch(str, "[^#]+")
	return {name=res(), damage=res() or 0, size=amount or 1}
end

local function stackToString(stack)
	return stack.name .. "#" .. stack.damage
end

local function initSchematics()
	drone.setStatusText("initializing")
	drone.setLightColor(0xFFCC00)

	for name, schematic in pairs(schematics) do
		schematic.height = #schematic.blocks
		schematic.width = 0
		schematic.depth = 0
		-- ingredients table
		schematic.ingredients = {}
		
		for _, layer in ipairs(schematic.blocks) do
			schematic.width = math.max(schematic.width, #layer)

			for _, line in ipairs(layer) do
				schematic.depth = math.max(schematic.depth, #line)
				
				-- count ingredients
				for i, block in ipairs(line) do
					if block ~= "" then
						local stack = stringToStack(block)
						block = stack.name .. "#" .. stack.damage
						line[i] = block

						local amount = schematic.ingredients[block]
						if amount then
							schematic.ingredients[block] = amount + 1
						else
							schematic.ingredients[block] = 1
						end
					end
				end
			end
		end
		
		-- add drop item to ingredients
		local stack = stringToStack(schematic.drop)
		local drop = stack.name .. "#" .. stack.damage
		schematic.drop = drop
		
		local amount = schematic.ingredients[drop]
		if amount then
			schematic.ingredients[drop] = amount + 1
		else
			schematic.ingredients[drop] = 1
		end
	end

	drone.setLightColor(0x00FF00)
	drone.setStatusText("idle")
end

local function getStacksInInventory(side)
	local nSlots
	if side then
		nSlots = inv.getInventorySize(side) or 0
	else
		nSlots = drone.inventorySize() or 0
	end
	
	local stacks = {}

	for i=1, nSlots do
		local stack
		if side then
			stack = inv.getStackInSlot(side, i)
		else
			stack = inv.getStackInInternalSlot(i)
		end
		
		if stack then
			stack.slot = i
			table.insert(stacks, stack)
		end
	end
	
	return stacks
end

-- checks precisely if the stacks and schematic ingredients match
local function checkIngredients(schematic, stacks)
	local existing = {}
	
	for _, stack in ipairs(stacks) do
		local block = stackToString(stack)
		local amount = existing[block]

		if amount then
			existing[block] = amount + stack.size
		else
			existing[block] = stack.size
		end
	end
	
	for block, amount in pairs(schematic.ingredients) do
		if not existing[block] or existing[block] ~= amount then
			return false
		end
	end
	
	return true
end

local function suckStacks(side, stacks)
	-- maybe return internal stack list for faster "findFirstBlockInInventory"
	--local blocksInternal = {}
	for _, stack in ipairs(stacks) do
		inv.suckFromSlot(side, stack.slot)
	end
end

-- finds the first slot with the given block
local function findFirstBlockInInventory(block)
	for i=1, drone.inventorySize() do
		local stack = inv.getStackInInternalSlot(i)
		if stack and stackToString(stack) == block then
			return i
		end
	end
	return nil
end

-- builds a schematic in the world. Assumes that the required blocks are in the drones inventory
local function build(schematic)
	local w, d, h = schematic.width, schematic.depth, schematic.height
	local layerX = targetX - math.floor(w / 2)
	local layerZ = targetZ - math.floor(d / 2)

	drone.setStatusText("building")
	drone.setLightColor(0xFFCC00)

	move(layerX, targetY + 1, layerZ)

	drone.setAcceleration(1.2)
	for y, layer in ipairs(schematic.blocks) do
		for x, line in ipairs(layer) do
			for z, block in ipairs(line) do
				if block ~= "" then
					local slot = findFirstBlockInInventory(block)
					if not slot then
						error("missing block: " .. block)
					end
					drone.select(slot)
					drone.place(sides.down)
				end
				
				if z ~= d then
					move(0, 0, 1)
				end
			end

			if x ~= w then
				move(1, 0, -d + 1)
			end
		end
		
		if y ~= h then
			move(-w + 1, 1, -d + 1)
		end
	end

	drone.setAcceleration(2.0)
	if 6 - h then
		move(0, 6 - h, 0)
	end
	drone.select(findFirstBlockInInventory(schematic.drop))
	drone.drop(sides.down)

	drone.select(1)
	home()

	drone.setLightColor(0x00FF00)
	drone.setStatusText("idle")
end

local function exec()
	while true do
		drone.setStatusText("check")
		drone.setLightColor(0xFFCC00)

		local stacks = getStacksInInventory(inventorySide)
		local schematic = nil

		for name, s in pairs(schematics) do
			local matched = checkIngredients(s, stacks)
			if matched then
				schematic = s
				break
			end
		end
		
		if schematic then
			suckStacks(inventorySide, stacks)
			build(schematic)
		end

		drone.setLightColor(0x00FF00)
		drone.setStatusText("idle")
		sleep(10)
	end
end

-- start program
initSchematics()
exec()