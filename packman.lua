local component = require("component")

if not component.isAvailable("internet") then
	error("this program requires an internet card")
end
local inet = require("internet")

local shell = require("shell")
local fs = require("filesystem")
local process = require("process")

-- repository url
local baseUrl = "https://bitbucket.org/Daarknes/minecraft-opencomputers/raw/master/"
--default library path
local libPath="/usr/lib"

local libraries = {
	core = {
		files = {
			"daarknes/core/class.lua",
			"daarknes/core/luaextensions.lua",
			"daarknes/core/colorprint.lua"
		},
		dependencies = {},
		name = "Core",
		description = "core library for my programs"
	},
	gui = {
		files = {
			"daarknes/gui/application.lua",
			"daarknes/gui/geom.lua",
			"daarknes/gui/guicomponent.lua",
			"daarknes/gui/painter.lua",
			"daarknes/gui/standardcomponents.lua"
		},
		dependencies = {"core"},
		name = "Gui",
		description = "my own gui-framework"
	}
}

local programs = {
	spawner = {
		files = {"spawner.lua"},
		dependencies = {"gui"},
		name = "Mob Duplicator Control",
		description = "gui-based control for the Industrialforegoing \"Mob Duplicator\""
	},
	ae2_autostock = {
		files = {"ae2_autostock.lua"},
		dependencies = {"core"},
		name = "Applied Energistics Autostock",
		description = "autostock program for Applied Energistics 2"
	},
	rs_autostock = {
		files = {"rs_autostock.lua"},
		dependencies = {"core"},
		name = "Refined Storage Autostock",
		description = "autostock program for Refined Storage"
	}
}


function string.fill(str, length)
	if #str < length then
		return str .. string.rep(" ", length - #str)
	elseif #str > length then
		return str:sub(1, length)
	else
		return str
	end
end


local function update()
	print("Updating the package manager ...")
	
	-- get the name of this programs (maybe it was renamed)
	local progName = process.info().path
	if not progName:find(".*%.lua") then
		progName = progName .. ".lua"
	end
	
	-- create update helper file
	local f = io.open("_packman_temp.lua", "w")
	f:write([[
local inet = require("internet")

local f = io.open("]] .. progName .. [[", "w")

for chunk in inet.request("]] .. baseUrl .. [[packman.lua") do
	f:write(chunk)
end
f:close()

os.execute("]] .. progName ..  [[ _finalizeUpdate")]])
	f:close()

	os.execute("_packman_temp.lua")
end

local function finalizeUpdate()
	local path = shell.resolve("_packman_temp.lua")
	if fs.remove(path) then
		print("Sucessfully updated the package manager.")
	else
		print("An unexpected error ocurred while updating the package manager.")
	end
end


-- TODO: package plan
local downloaded = {}
local function installLib(name, force)
	if libraries[name] and not downloaded[name] then
		table.insert(downloaded, name)

		for _, req in ipairs(libraries[name].dependencies) do
			if not installLib(req, force) then
				return false
			end
		end

		for _, file in ipairs(libraries[name].files) do
			local path = fs.concat(libPath, file)
			-- check if this library already exists
			if fs.exists(path) and not force then
				print("files for library \"" .. name .. "\" already exist. To force-overwrite them use the '-f' option.")
				return false
			end
			
			local folder = fs.path(path)
			if not fs.exists(folder) then
				fs.makeDirectory(folder)
			end

			local f = io.open(path, "w")
			for chunk in inet.request(baseUrl .. file) do
				f:write(chunk)
			end
			f:close()
		end
		
		print("successfully installed libary \"" .. name .. "\"")
	else
		print("no library named \"" .. name .. "\"")
	end
	
	return true
end

local function installProgram(name, force)
	if programs[name] then
		for _, req in ipairs(programs[name].dependencies) do
			if not installLib(req, force) then
				return false
			end
		end

		for _, file in ipairs(programs[name].files) do
			-- check if this program already exists (shell.resolve is needed because of relative paths)
			if fs.exists(shell.resolve(file)) and not force then
				print("files for program \"" .. name .. "\" already exist. To force-overwrite them use the '-f' option.")
				return false
			end

			local f = io.open(file, "w")
			for chunk in inet.request(baseUrl .. file) do
				f:write(chunk)
			end
			f:close()
		end

		print("successfully installed program \"" .. name .. "\"")
	else
		print("no program named \"" .. name .. "\"")
	end
end

uninstalled = {}
local function uninstall(name)
	local pkg = libraries[name] or programs[name]
	if pkg == nil then
		print("no library or program named \"" .. name .. "\"")
		return false
	end
	local isLib = libraries[name] and true
	
	if not uninstalled[name] then
		table.insert(uninstalled, name)

		for _, req in ipairs(pkg.dependencies) do
			if not uninstall(req) then
				return false
			end
		end

		for _, file in ipairs(pkg.files) do
			local path
			if isLib then
				path = fs.concat(libPath, file)
			else
				path = file
			end

			path = shell.resolve(path)
			if not fs.remove(path) then
				print("failed to remove \"" .. path .. "\" when uninstalling \"" .. name .. "\"")
				return false
			end
		end
		
		print("successfully uninstalled \"" .. name .. "\"")
	end

	return true
end

local function showHelp()
	width = 32
	indentStr = string.rep(" ", width+1)
	
	print("Daarknes' OpenComputers library and program manager.")
	print("Usage:")
	print(string.fill("'packman list'", width) .. " lists all available libraries and programs.")
	print(string.fill("'packman install <name> [-f]'", width) .. " installs a library or program 'name' and its dependencies.\n" ..
		indentStr .. "supply option '-f' to force-overwrite existing files.")
	print(string.fill("'packman uninstall <name>'", width) .. " uninstall library or program 'name' and its dependencies.")
	print(string.fill("'packman update'", width) .. " (re-download) the newest version of this package manager")
end

local function showList()
	print("+---------------------------------+")
	print("| available libraries:            |")
	print("+---------------------------------+")
	for name, lib in pairs(libraries) do
		print(name .. " = \"" .. lib.name .. "\":\n\t" .. lib.description)
	end

	print("\n+---------------------------------+")
	print("| available programs:             |")
	print("+---------------------------------+")
	for name, prog in pairs(programs) do
		print(name .. " = \"" .. prog.name .. "\":\n\t" .. prog.description)
	end
end


local function handleArguments(args, options)
	if args[1] == "_finalizeUpdate" then
		finalizeUpdate()
	elseif args[1] == "update" then
		update()
	elseif args[1] == "list" then
		showList()
	elseif args[1] == "install" and args[2] ~= nil then
		if libraries[args[2]] then
			installLib(args[2], options.f)
		elseif programs[args[2]] then
			installProgram(args[2], options.f)
		else
			print("\"" .. args[2] .. "\" is not a valid library or program. Use 'packman list' for a list of all available libraries and programs.")
		end
	elseif args[1] == "uninstall" and args[2] ~= nil then
		uninstall(args[2])
	else
		showHelp()
	end
end


local args, options = shell.parse(...)
handleArguments(args, options)