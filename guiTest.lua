package.loaded["daarknes.core.class"] = nil
package.loaded["daarknes.core.luaextensions"] = nil
package.loaded["daarknes.gui.application"] = nil
package.loaded["daarknes.gui.painter"] = nil
package.loaded["daarknes.gui.geom"] = nil
package.loaded["daarknes.gui.guicomponent"] = nil
package.loaded["daarknes.gui.standardcomponents"] = nil
package.loaded["daarknes.gui.logger"] = nil

require("daarknes.gui.logger")
require("daarknes.core.luaextensions")
require("daarknes.gui.guicomponent")
require("daarknes.gui.standardcomponents")
local app = require("daarknes.gui.application")

local window = GuiComponent(0, 0, 100, 50)
local b = Button(1, 1, nil, nil, "Test")
window:addChild(b)

app.init(window)
app.exec()