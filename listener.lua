local modem = component.proxy(component.list("modem")())
modem.open(1)

while true do
	local e, _, _, _, _, msg = computer.pullSignal()
	if e == "modem_message" then
		local reply = load(msg)()
		modem.broadcast(1, tostring(reply))
	end
end