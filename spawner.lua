--[[
package.loaded["daarknes.core.class"] = nil
package.loaded["daarknes.core.luaextensions"] = nil
package.loaded["daarknes.gui.application"] = nil
package.loaded["daarknes.gui.painter"] = nil
package.loaded["daarknes.gui.geom"] = nil
package.loaded["daarknes.gui.guicomponent"] = nil
package.loaded["daarknes.gui.standardcomponents"] = nil
package.loaded["daarknes.gui.logger"] = nil
--]]

local component = require("component")
local sides = require("sides")
require("daarknes.core.luaextensions")
local app = require("daarknes.gui.application")
require("daarknes.gui.standardcomponents")

-- debug
--require("daarknes.gui.logger")

-- configuration -----------------------------------------------------------------
local transposer = component.transposer
local spawnerSide = sides.up
local inventorySide = sides.east
local redstone = component.redstone
local redstoneSide = sides.up


----------------------------------------------------------------------------------
local window = GuiComponent(0, 0, 50, 25)
window:setBGColor(0xDDDDDD)
local mobView = ListView(23, 0, 27, 25)
window:addChild(mobView)

window:addChild(Label(2, 1, "Spawning:"))
local lSpawning = Label(2, 3, "", 19, 3)
window:addChild(lSpawning)


local mobs = {}
local currentIndex = nil

local function getMobName(stack)
	-- extract the mob name inside the parenthesis
	local label = string.gmatch(stack.label, "%((.+)%)")()
	-- remove leading mod names (cut after last '.')
	for chunk in string.gmatch(label, "[^%.]+") do
		label = chunk
	end
	return label
end

local function getMobs(transposer, side, minSlot, maxSlot)
	minSlot = minSlot or 1
	maxSlot = maxSlot or transposer.getInventorySize(side)
	
	local mobs = {}
	for i=minSlot, maxSlot do
		local stack = transposer.getStackInSlot(side, i)
		if stack and stack.name == "industrialforegoing:mob_imprisonment_tool" then
			local mob = {name=getMobName(stack), slot=i}
			mobs[#mobs + 1] = mob
		end
	end
	
	return mobs
end

local function spawnMob(transposer, mobIndex)
	if currentIndex == mobIndex then
		return
	end

	if currentIndex then
		if not transposer.transferItem(spawnerSide, inventorySide, nil, 7, mobs[currentIndex].slot) then
			error("can't suck in item from spawner, clear spawner first, then add new mobs")
			return
		end
	end

	mobName = ""
	if mobIndex then
		transposer.transferItem(inventorySide, spawnerSide, nil, mobs[mobIndex].slot)
		mobName = mobs[mobIndex].name
	end

	currentIndex = mobIndex
	lSpawning:setText(mobName)
end

local function refresh()
	if currentIndex then
		spawnMob(transposer, nil)
	end

	mobs = getMobs(transposer, inventorySide)

	local width = 0
	for _, mob in ipairs(mobs) do
		width = math.max(width, #mob.name)
	end
	width = math.min(width, mobView:getContentRect().width - 3)

	-- sort function (by mob name)
	local f = function(a, b)
		return a.name < b.name
	end
	table.sort(mobs, f)
	
	mobView:clear()
	for i, mob in ipairs(mobs) do
		local f = partial(spawnMob, transposer, i)
		local button = Button(0, 0, width + 2, 3, mob.name, f)
		mobView:addChild(button)
	end
	mobView:update()
end


-- buttons
local bActivate = Button(2, 12, 19, 3, "Activate Spawner", nil, true)
local function toggleSpawner()
	if bActivate.state == 1 then
		redstone.setOutput(redstoneSide, 15)
	else
		redstone.setOutput(redstoneSide, 0)
	end
end
bActivate:setAction(toggleSpawner)

window:addChild(Button(2, 8, 19, 3, "Clear Spawner", partial(spawnMob, transposer, nil)))
window:addChild(bActivate)
window:addChild(Button(2, 21, 19, 3, "Refresh", refresh))


-- clear spawner
transposer.transferItem(spawnerSide, inventorySide)
-- load all mobs
refresh()

-- start application
app.init(window)
app.exec()

-- clear spawner
if currentIndex then
	spawnMob(transposer, nil)
end