local component = require("component")
local modem = component.modem
local computer = require("computer")

local f = io.open("robotBuilder.lua", "r")
local code = f:read("*all")
f:close()
modem.broadcast(1, code)


modem.open(1)
local waiting = true

while waiting do
	local e, _, _, _, _, msg = computer.pullSignal()
	if e == "modem_message" then
		print(msg)
		waiting = false
	end
end