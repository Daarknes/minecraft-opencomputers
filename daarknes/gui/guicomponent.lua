require("daarknes.core.class")
require("daarknes.gui.application")
require("daarknes.gui.geom")

GuiComponent = class()
function GuiComponent:__init(x, y, width, height)
	self.rect = Rect(x or 0, y or 0, width or -1, height or -1)
	self.parent = nil
	self.children = {}
	self.visible = true
	self._bgColor = math.random(0x000000, 0xFFFFFF)
end

function GuiComponent:x()
	return self.rect.x
end

function GuiComponent:y()
	return self.rect.y
end

function GuiComponent:pos()
	return self.rect.x, self.rect.y
end

function GuiComponent:setPos(x, y)
	self.rect:setPos(math.floor(x), math.floor(y))
end

function GuiComponent:width()
	return self.rect.width
end

function GuiComponent:setWidth(width)
	self.rect.width = math.floor(width)
end

function GuiComponent:height()
	return self.rect.height
end

function GuiComponent:setHeight(height)
	self.rect.height = math.floor(height)
end

function GuiComponent:size()
	return self.rect:size()
end

function GuiComponent:setSize(width, height)
	self.rect:setSize(math.floor(width), math.floor(height))
end

function GuiComponent:setVisible(visible)
	self.visible = visible
	self:update()
end

function GuiComponent:getLocalRect()
	return Rect(0, 0, self.rect.width, self.rect.height)
end

function GuiComponent:addChild(child)
	assert(child, "child can't be nil")
	if child == self then
		error("can't add self as child")
	end
	-- two different GuiComponents shouldn't have the same child
	if child.parent then
        if child.parent == self then
			return
		end
		child.parent:removeChild(child)
	end
    table.insert(self.children, child)
    child.parent = self
	local childX, childY = child:pos()
	child:setPos(self.rect.x + childX, self.rect.y + childY)
end

function GuiComponent:removeChild(child)
    if not (child:getParent() == self) then
		return
	end

    for idx, c in pairs(self.children) do
		if c == child then
			table.remove(self.children, idx)
			child.parent = nil
			break
		end
	end
end

function GuiComponent:setBGColor(bgColor)
	self._bgColor = bgColor
end

function GuiComponent:update(globalChange)
	-- global change (e.g. pos or dim changed) -> need to redraw from parent
	if globalChange then
		application.update(self.parent)
	-- only changed internally -> only self redraw necessary
	else
		application.update(self)
	end
end

-- internal function for handling a mouse click on this component and/or its children
function GuiComponent:_handleMouseEvent(x, y, button, player)
	if self:mouseEvent(x - self:x(), y - self:y(), button, player) then
		return self
	end

	for _, child in ipairs(self.children) do
        if child.rect:contains(x, y) then
			local result = child:_handleMouseEvent(x, y, button, player)
			if result then
				return result
			end
        end
    end
	
	return nil
end

-- override this to implement own mouse-click handling. It can be assumed that (x, y) is contained in this components rect.
function GuiComponent:mouseEvent(x, y, button, player)
    return false
end

-- internal function for handling a mouse drag on this component and/or its children
function GuiComponent:_handleDragEvent(x, y, button, player)
	if self:dragEvent(x, y, button, player) then
		return true
	end

	for _, child in ipairs(self.children) do
        if child.rect:contains(x, y) and child:_handleDragEvent(x, y, button, player) then
            return true
        end
    end
	
	return false
end

-- override this to implement own mouse-drag handling. It can be assumed that (x, y) is contained in this components rect.
function GuiComponent:dragEvent(x, y, button, player)
    return false
end

-- internal function for handling scroll-wheel events on this component and/or its children
function GuiComponent:_handleScrollEvent(x, y, direction, player)
	if self:scrollEvent(x, y, direction, player) then
		return true
	end

	for _, child in ipairs(self.children) do
        if child.rect:contains(x, y) and child:_handleScrollEvent(x, y, direction, player) then
            return true
        end
    end
	
	return false
end

-- override this to implement own scroll-wheel handling. It can be assumed that (x, y) is contained in this components rect.
function GuiComponent:scrollEvent(x, y, direction, player)
    return false
end

-- internal function for drawing this component and its children
function GuiComponent:_redraw(painter)
	self:draw(painter)

	-- draw children
	for _, child in ipairs(self.children) do
		local unionRect = self.rect:union(child.rect)
		if child.visible and unionRect:isValid() then
			painter:setOffset(child:pos())
			painter:setClippingArea(unionRect)
			child:_redraw(painter)
		end
    end
end

function GuiComponent:draw(painter)
    painter:drawRectangle(self:getLocalRect(), self._bgColor)
end