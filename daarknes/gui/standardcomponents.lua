require("daarknes.core.class")
require("daarknes.core.luaextensions")
require("daarknes.gui.guicomponent")
local event = require("event")

-- Label ------------------------------------------------------------------------------------------
Label = class(GuiComponent)
function Label:__init(x, y, text, width, height, textColor, bgColor)
	assert(type(text) == "string")
	GuiComponent.__init(self, x, y, width or #text, height or 1)

    self.text = text
	self.textColor = textColor or 0x000000
	self.bgColor = bgColor
end

function Label:setText(text)
	self.text = text
	self:update()
end

function Label:draw(painter)
	painter:drawTextRect(self:getLocalRect(), self.text, self.textColor, self.bgColor)
end


-- Button ------------------------------------------------------------------------------------------
Button = class(GuiComponent)
function Button:__init(x, y, width, height, label, func, toggle, textColor)
	GuiComponent.__init(self, x, y, width or #label, height or 1)
	self.label = label
	self.textColor = textColor or 0xFFFFFF

	self.state = 0
	self.toggle = toggle or false

	self._func = func
end

function Button:setLabel(label)
	self.label = label
	self:update()
end

function Button:setAction(func)
	self._func = func
end

function Button:_performAction()
	if self._func then
		self._func()
	end

	if not self.toggle then
		self.state = 0
		self:update()
	end
end

function Button:mouseEvent(x, y, button, player)
	if button == 0 then
		if self.toggle then
			self.state = 1 - self.state
			self:update()
			self:_performAction()
		else
			self.state = 1
			self:update()
			event.timer(0.5, partial(Button._performAction, self), 1)
		end

		return true
	end
end

function Button:draw(painter)
	local color
	if self.state == 1 then
		color = 0x00FF00
	else
		color = 0xFF0000
	end

	painter:drawTextRect(self:getLocalRect(), self.label, self.textColor, color)
end

-- Scrollbar ------------------------------------------------------------------------------------------
Scrollbar = class(GuiComponent)
function Scrollbar:__init(x, y, width, height, orientation)
	GuiComponent.__init(self, x, y, width, height)
	self._orientation = orientation or 1
	self.value = 0
	self.onChange = nil

	if orientation == 0 then
		self._barLength = 4
		self._steps = width - 3
	else
		self._barLength = 3
		self._steps = height - 2
	end
end

function Scrollbar:setValue(value)
	if self.value ~= value then
		self.value = value
		if self.onChange then
			self.onChange(value)
		end
		self:update()
		return true
	else
		return false
	end
end

function Scrollbar:mouseEvent(x, y, button, player)
	if button == 0 then
		local value
		if self._orientation == 0 then
			value = math.clamp((x - self._barLength / 2.0) / (self:width() - self._barLength), 0, 1)
		else
			value = math.clamp((y - self._barLength / 2.0) / (self:height() - self._barLength), 0, 1)
		end

		return self:setValue(value)
	end
	
	return false
end

function Scrollbar:dragEvent(x, y, button, player)
	self:mouseEvent(x, y, button, player)
end

function Scrollbar:scrollEvent(x, y, direction, player)
	local length
	if self._orientation == 0 then
		length = self:width() - self._barLength
	else
		length = self:height() - self._barLength
	end

	local value = math.clamp(self.value - direction / length, 0, 1)
	return self:setValue(value)
end

function Scrollbar:draw(painter)
	painter:drawRectangle(self:getLocalRect(), 0x909090)

	-- horizontal
	if self._orientation == 0 then
		barX = math.floor(self.value * (self:width() - self._barLength))
		painter:drawRectangle(Rect(barX, 0, self._barLength, self:height()), 0x64c8ff)
	-- vertical
	else
		barY = math.floor(self.value * (self:height() - self._barLength))
		painter:drawRectangle(Rect(0, barY, self:width(), self._barLength), 0x64c8ff)
	end
end


-- ListView ------------------------------------------------------------------------------------------
ListView = class(GuiComponent)
function ListView:__init(x, y, width, height)
	GuiComponent.__init(self, x, y, width, height)
	self._scrollbar = Scrollbar(width - 4, 0, 4, height)
	self._scrollbar.onChange = partial(ListView.scrollContent, self)
	GuiComponent.addChild(self, self._scrollbar)
	
	self._contentHeight = 1
end

function ListView:getContentRect()
	return Rect(0, 0, self:width() - 4, self:height())
end

function ListView:addChild(child)
	local x = math.floor((self:width() - 4 - child:width()) / 2.0)

	child:setPos(x, self._contentHeight)
	GuiComponent.addChild(self, child)

	self._contentHeight = self._contentHeight + child:height() + 1
end

function ListView:clear(update)
	self.children = {self._scrollbar}
	self._contentHeight = 1

	if update then
		self:update()
	end
end

function ListView:scrollContent(value)
	local offset = math.max(value * (self._contentHeight - self:height()), 0)
	local y = 1 - offset

	for _, child in ipairs(self.children) do
		if child ~= self._scrollbar then
			child:setPos(child:x(), y)
			
			y = y + child:height() + 1
		end
	end
	
	self:update()
end

function ListView:scrollEvent(x, y, direction, player)
	for _, child in ipairs(self.children) do
        if child.rect:contains(x, y) and child:_handleScrollEvent(x, y, direction, player) then
            return true
        end
    end

	return self._scrollbar:scrollEvent(x, y, direction, player)
end

function ListView:draw(painter)
	painter:drawRectangle(self:getContentRect(), 0xB0B0B0)
end


-- PageView ------------------------------------------------------------------------------------------
local tabWidth, tabHeight = 20, 3

PageView = class(GuiComponent)
function PageView:__init(x, y, width, height)
	GuiComponent.__init(self, x, y, width, height)
	self._pages = {}
	self._current = 1
end

function PageView:switchPage(pageIdx)
	if pageIdx ~= self._current and pageIdx > 0 and pageIdx <= #self._pages then
		self._current = pageIdx
		self:update()
	end
end

function PageView:addPage(pageName)
	local dummy = GuiComponent(0, tabHeight, self:width(), self:height() - tabHeight)
	table.append(self._pages, {name=pageName, widget=dummy})
	return dummy
end

function PageView:mouseEvent(x, y, button, player)
	if button == 0 and y < tabHeight and x < #self._pages * tabWidth then
		local pageIdx = math.floor(x / tabWidth) + 1
		self:switchPage(pageIdx)
		return true
	else
		local pageWidget = self._pages[self._current].widget
		if pageWidget.rect:contains(x, y) and pageWidget:_handleMouseEvent(x, y, button, player) then
            return true
		else
			return false
        end
	end
end

function PageView:draw(painter)
	for i, page in ipairs(self._pages) do
		local x = (i - 1) * tabWidth
		local rect = Rect(x, 0, tabWidth, tabHeight)

		if i == self._current then
			painter:drawTextRect(rect, page.name, 0x000000, 0xAAAAAA)

			local oldClipRect = painter:getClippingArea()
			painter:setClippingArea(page.widget.rect)
			page.widget:_redraw(painter)
			painter:setClippingArea(oldClipRect)
		else
			painter:drawTextRect(rect, page.name, 0x000000, 0x555555)
		end
	end
end