require("daarknes.core.class")

-- utility class for a two dimensional vector (tuple)
Vec2D = class()
function Vec2D:__init(x, y)
  self.x = x or 0
  self.y = y or 0
end

function Vec2D:__add(other)
  return Vec2D(self.x + other.x, self.y + other.y)
end

function Vec2D:__sub(vec)
  return Vec2D(self.x - vec.x, self.y - vec.y)
end

function Vec2D:__eq(vec)
  return self.x == vec.x and self.y == vec.y
end

function Vec2D:__len()
  return 2
end

function Vec2D:unpack()
  return self.x, self.y
end

function Vec2D:__tostring()
  return "Vec2D(" .. self.x .. ", " .. self.y .. ")"
end



-- utility class for a rectangle
Rect = class()
function Rect:__init(x, y, width, height)
  self.x = x or 0
  self.y = y or 0
  self.width = width or -1
  self.height = height or -1
end

function Rect:isValid()
  return self.width > 0 and self.height > 0
end

function Rect:pos()
  return self.x, self.y
end

function Rect:setPos(x, y)
  self.x = x
  self.y = y
end

function Rect:size()
  return self.width, self.height
end

function Rect:setSize(width, height)
  self.width = width
  self.height = height
end

function Rect:contains(x, y)
  local flagX = x >= self.x and x <= self.x + self.width
  local flagY = y >= self.y and y <= self.y + self.height
  return flagX and flagY
end

function Rect:offsetted(x, y)
  return Rect(self.x + x, self.y + y, self.width, self.height)
end

function Rect:adjusted(x, y, width, height)
  return Rect(self.x + x, self.y + y, self.width + width, self.height + height)
end

function Rect:union(other)
  local xMin = math.max(self.x, other.x)
  local xMax = math.min(self.x + self.width, other.x + other.width)
  local yMin = math.max(self.y, other.y)
  local yMax = math.min(self.y + self.height, other.y + other.height)
  return Rect(xMin, yMin, xMax - xMin, yMax - yMin)
end

function Rect:unpack()
  return self.x, self.y, self.width, self.height
end

Rect.__type = "rect"

function Rect:__tostring()
  return "Rect(" .. tostring(Vec2D(self.x, self.y)) .. ", (" .. self.width .. ", " .. self.height .. "))"
end