require("daarknes.core.class")
require("daarknes.gui.geom")

Painter = class()
function Painter:__init(gpu)
	assert(gpu, "gpu can't be nil")
	self.gpu = gpu

	self.clipRect = Rect()
	self.offsetX = 0
	self.offsetY = 0
end

function Painter:getClippingArea()
	return self.clipRect
end

function Painter:setClippingArea(rect)
	self.clipRect = rect
end

function Painter:getOffset()
	return self.offsetX, self.offsetY
end

function Painter:setOffset(x, y)
	self.offsetX = x
	self.offsetY = y
end

function Painter:drawRectangle(rect, color, borderColor)
	bgColor = color or self.colorBG
	local restoreColorBG = self.gpu.getBackground()

	local drawRect = self.clipRect:union(rect:offsetted(self.offsetX, self.offsetY))
	if drawRect:isValid() then
		-- +1 because fuck lua indexing
		local x, y, w, h = drawRect:offsetted(1, 1):unpack()

	if borderColor then
		-- draw borders
		self.gpu.setBackground(borderColor)
		self.gpu.fill(x, y, w, 1, " ")          -- top
		self.gpu.fill(x, y + h - 1, w, 1, " ")  -- bottom
		self.gpu.fill(x, y, 1, h, " ")          -- left
		self.gpu.fill(x + w - 1, y, 1, h, " ")  -- right
		-- draw inner
		self.gpu.setBackground(bgColor)
		self.gpu.fill(x + 1, y + 1, w - 2, h - 2, " ")
    else
		self.gpu.setBackground(bgColor)
		self.gpu.fill(x, y, w, h, " ")
    end

	self.gpu.setBackground(restoreColorBG)
  end
end

function Painter:drawText(x, y, text, textColor, bgColor)
	assert(type(text) == "string")
	
	local textRect = Rect(x, y, #text, 1)
	local drawRect = self.clipRect:union(textRect:offsetted(self.offsetX, self.offsetY))

	if drawRect:isValid() then
		textColor = textColor or 0x000000
		local restoreColorFG = self.gpu.getForeground()
		local restoreColorBG = self.gpu.getBackground()

		self.gpu.setForeground(textColor)
		if bgColor then
			self.gpu.setBackground(bgColor)
		end

		-- +1 because fuck lua indexing
		self.gpu.set(drawRect.x + 1, drawRect.y + 1, text:sub(1, drawRect.width))
		
		self.gpu.setForeground(restoreColorFG)
		if bgColor then
			self.gpu.setBackground(restoreColorBG)
		end
	end
end

function Painter:drawTextRect(rect, text, textColor, bgColor)
	rect = rect:offsetted(self.offsetX, self.offsetY)
	local drawRect = self.clipRect:union(rect)

	if drawRect:isValid() then
		text = string.fill(text, drawRect.width)

		local restoreColorFG = self.gpu.getForeground()
		local restoreColorBG = self.gpu.getBackground()
		self.gpu.setForeground(textColor)
		if bgColor then
			self.gpu.setBackground(bgColor)
		end

		local x, y, w, h = drawRect:unpack()
		local idx = math.floor((rect.height - 1) / 2) - (y - rect.y)
		--lprint(text, self.clipRect, rect, drawRect)

		for i=0, h-1 do
			-- +1 because fuck lua indexing  
			if i == idx then
				self.gpu.set(x + 1, y + 1 + i, text)
			else
				self.gpu.fill(x + 1, y + 1 + i, w, 1, " ")
			end
		end
	
		self.gpu.setForeground(restoreColorFG)
		if bgColor then
			self.gpu.setBackground(restoreColorBG)
		end
	end
end