require("daarknes.core.luaextensions")
require("daarknes.gui.painter")

local component = require("component")
local computer = require("computer")
local term = require("term")
local event = require("event")

local redrawFrequency = 0.1

application = {
	rootComponent = nil,
	gpu = component.gpu,
	painter = Painter(component.gpu),
	screen = component.screen,
	timer = nil,
	redrawQueue = {}
}

function application.getResolution()
	return application.gpu.maxResolution()
end

function application.init(rootComponent)
	assert(rootComponent, "root component can't be  nil")
	application.rootComponent = rootComponent

	term.setCursorBlink(false)
	term.clear()

	application.restore = {
		resolution = {application.gpu.getResolution()},
		bgColor = application.gpu.getBackground(),
		fgColor = application.gpu.getForeground(),
		precise = application.screen.isPrecise()
	}

	-- root position is always at (0, 0)
	rootComponent:setPos(0, 0)
	-- check that width and height aren't exceeding max resolution
	maxWidth, maxHeight = application.gpu.maxResolution()
	if rootComponent:width() > maxWidth then
		rootComponent:setWidth(maxWidth)
	end
	if rootComponent:height() > maxHeight then
		rootComponent:setHeight(maxHeight)
	end
	-- root component must be visible
	rootComponent:setVisible(true)

	application.gpu.setResolution(rootComponent:width(), rootComponent:height())
	application.gpu.setBackground(0xFFFFFF)
	application.gpu.setForeground(0x000000)

	application.screen.setPrecise(true)

	application.painter:setClippingArea(rootComponent.rect)
	application.update()
end

local function isInherited(parent, comp)
	while comp.parent do
		comp = comp.parent
		if comp == parent then
			return true
		end
	end
	return false
end

function application.update(comp)
	comp = comp or application.rootComponent
	if not comp.parent and comp ~= application.rootComponent then
		return
	end

	for _, c, rm in ripairs(application.redrawQueue) do
		-- already in queue or a higher level component already requested an update
		if c == comp or isInherited(c, comp) then
			return
		-- a lower level component requested an update -> remove it
		elseif isInherited(comp, c) then
			rm()
		end
	end
	
	application.redrawQueue[#application.redrawQueue + 1] = comp
end

function application._redraw()
	if #application.redrawQueue > 0 then
		local clipRectOld = application.painter:getClippingArea()
		local offsetX, offsetY = application.painter:getOffset()

		for _, c in ipairs(application.redrawQueue) do
			local rect = c.rect
			if c.parent then
				rect = c.parent.rect:union(rect)
			end
			application.painter:setClippingArea(c.rect)
			application.painter:setOffset(c:pos())
			c:_redraw(application.painter)
		end

		application.painter:setClippingArea(clipRectOld)
		application.painter:setOffset(offsetX, offsetY)

		-- clear redraw queue
		application.redrawQueue = {}
	end
end


function application.exec()
	local eventFilter = {"touch", "drag", "scroll", "screen_resized", "interrupted"}
	application._running = true
	application.timer = event.timer(redrawFrequency, application._redraw, math.huge)

	while application._running do
		e = {event.pullMultiple(table.unpack(eventFilter))}
		application._handleEvent(table.unpack(e))
	end
  
	application._cleanUp()
end

-- variable to track last component that was "touched"
local _lastTouch = nil

function application._handleEvent(eventId, address, ...)
	--lprint("Event received:", eventId)
	if eventId == "interrupted" then
		application._running = false
	-- note: when precise-mode is active we don't need to subtract 1 since coords then start at (0, 0)
	elseif eventId == "touch" then
		local x, y, button, player = ...
		_lastTouch = application.rootComponent:_handleMouseEvent(x, y, button, player)
	elseif eventId == "drag" and _lastTouch then
		local x, y, button, player = ...
		x = x - _lastTouch:x()
		y = y - _lastTouch:y()
		_lastTouch:_handleDragEvent(x, y, button, player)
	elseif eventId == "scroll" then
		local x, y, direction, player = ...
		application.rootComponent:_handleScrollEvent(x, y, direction, player)
	end
end

function application._cleanUp()
	event.cancel(application.timer)
	application.gpu.setResolution(table.unpack(application.restore.resolution))
	application.gpu.setBackground(application.restore.bgColor)
	application.gpu.setForeground(application.restore.fgColor)
	application.screen.setPrecise(application.restore.precise)
	term.clear()
end

return application