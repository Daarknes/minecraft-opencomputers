local component = require("component")
local term = require("term")

--[[
local logs = {}

local function logMsg(msg)
	logs[#logs + 1] = tostring(msg) .. "\n"
end

function showLog()
	print(table.concat(logs))
	logs = {}
end

lprint = logMsg
]]

local gpu2 = component.proxy(component.get("c473"))  -- the non-primary GPU
local screen2 = component.get("0c4d")  -- the non-primary screen
gpu2.bind(screen2)

local y = 1
local w, h = gpu2.getResolution()

local function clear()
	gpu2.fill(1, 1, w, h, " ")
	y = 1
end

clear()

lprint = function(...)
	if y > h then
		clear()
	end
	
	text = {}
	for _, t in ipairs({...}) do
		text[#text + 1] = tostring(t) .. "   "
	end

	gpu2.set(1, y, table.concat(text))
	y = y + 1
end