--[[
	This library provides a colored print for screens.
]]
local component = require("component")
local term = require("term")
local gpu = component.getPrimary("gpu")

function colored(text, color)
	checkArg(2, color, "number")
	return {type="coloredText", text=tostring(text), color=color}
end

function colorprint(...)
	for i, v in ipairs({...}) do
		if type(v) == "table" and v.type == "coloredText" then
			local oldColor, isPalette = gpu.setForeground(v.color)
			term.write(v.text)
			gpu.setForeground(oldColor, isPalette)
		else
			term.write(tostring(v))
		end
	end
	term.write("\n")
end