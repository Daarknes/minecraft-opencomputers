--[[
	This library extends a few default Lua methods
	and adds some features that haven't been implemented yet.
]]

local filesystem = require("filesystem")


------------------------------------------------------ general ------------------------------------------------------

--[[ https://stackoverflow.com/questions/12394841/safely-remove-items-from-an-array-table-while-iterating
-- iterator that allows removing while iterating.
-- Try not to use break when using this function; it may cause the array to be left with empty slots.
-- usage:
for i, v, rm in ripairs(array) do
	if condition then rm()
end
]]
function ripairs(t)
    local ci = 0
    local remove = function()
        t[ci] = nil
    end
    return function(t, i)
        i = i+1
        ci = i
        local v = t[i]
        if v == nil then
            local rj = 0
            for ri = 1, i-1 do
                if t[ri] ~= nil then
                    rj = rj+1
                    t[rj] = t[ri]
                end
            end
            for ri = rj+1, i do
                t[ri] = nil
            end
            return
        end
        return i, v, remove
    end, t, ci
end

function partial(f, ...)
	checkArg(1, f, "function")
	local pargs = {...}
	return function(...)
		return f(table.unpack(table.imerge(pargs, {...})))
	end
end


-------------------------------------------------- Table extensions --------------------------------------------------

function table.append(tbl, ...)
	local args = {...}
	for i=1, #args do
		tbl[#tbl + 1] = args[i]
	end
end

function table.size(tbl, countNil)
	local n = 0
	for k, v in pairs(tbl) do
		if v or countNil then
			n = n + 1
		end
	end
	return n
end

function table.isEmpty(tbl)
	return next(tbl) == nil
end

local function doSerialize(tbl, indentSymbol, recursionLimit, recursionDepth)
	local indentStr = string.rep(indentSymbol, recursionDepth + 1)
	local res = {"{\n"}

	for key, value in pairs(tbl) do
		table.append(res, indentStr)
		local kType, vType = type(key), type(value)
		local stringValue = tostring(value)
		
		if kType == "number" then
			table.append(res, "[", key, "]")
		elseif kType == "string" then
			if key:match("^%a") and key:match("^[%w%_]+$") then
				table.append(res, key)
			else
				table.append(res, "[\"", key, "\"]")
			end
		end

		table.append(res, " = ")
		
		if vType == "number" or valueType == "boolean" or valueType == "nil" then
			table.append(res, stringValue)
		elseif vType == "string" then
			table.append(res, "\"", stringValue, "\"")
		elseif vType == "function" then
			table.append(res, stringValue, "()")
		elseif vType == "table" then
			-- check for userdata (AE2)
			if value.type == "userdata" then
				table.append(res, "userdata \"", stringValue, "\" ")
				value.type = nil
			end

			-- prevent loop of death
			if value.proxy ~= nil then
				table.append(res, stringValue)
			-- recursion
			elseif recursionDepth < recursionLimit then
				table.append(res, table.concat(doSerialize(value, indentSymbol, recursionLimit, recursionDepth + 1)))
			else
				table.append(res, "[…]")
			end
		end
		
		table.append(res, ",", "\n")
	end
	
	-- remove last ","
	if #res > 2 then
		table.remove(res, #res - 1)
	end
	
	table.append(res, string.rep(indentSymbol, recursionDepth), "}")
	return res
end

function table.toString(tbl, recursionLimit, indentSymbol)
	checkArg(1, tbl, "table")
	indentSymbol = indentSymbol or "  "

	return table.concat(doSerialize(tbl, indentSymbol, recursionLimit or math.huge, 0))
end

function table.toFile(path, tbl, recursionLimit, indentSymbol, appendToFile)
	checkArg(1, path, "string")
	checkArg(2, tbl, "table")
	indentSymbol = indentSymbol or "  "

	filesystem.makeDirectory(filesystem.path(path) or "")
	
	local file, reason = io.open(path, appendToFile and "a" or "w")
	if file then
		file:write(table.toString(tbl, recursionLimit, indentSymbol))
		file:close()
	else
		error("Failed to open file for writing: " .. tostring(reason))
	end
end

local function doTableCopy(source, destination, deepcopy)
	for key, value in pairs(source) do
		if deepcopy and type(value) == "table" then
			destination[key] = {}
			doTableCopy(source[key], destination[key], deepcopy)
		else
			destination[key] = value
		end
	end
end

function table.copy(tbl, deepcopy)
	checkArg(1, tbl, "table")

	local copyTable = {}
	doTableCopy(tbl, copyTable, deepcopy)
	return copyTable
end

function table.merge(tbl1, tbl2)
	local res = {}
	for k, v in pairs(tbl1) do
		res[k] = v
	end
	for k, v in pairs(tbl2) do
		res[k] = v
	end
	return res
end

function table.imerge(tbl1, tbl2)
	local res = {}
	for _, v in ipairs(tbl1) do
		res[#res + 1] = v
	end
	for _, v in ipairs(tbl2) do
		res[#res + 1] = v
	end
	return res
end

function table.slice(tbl, start, stop, step)
	local sliced = {}

	for i = start or 1, stop or #tbl, step or 1 do
		sliced[#sliced+1] = tbl[i]
	end

	return sliced
end


-------------------------------------------------- math extensions ---------------------------------------------------

function math.clamp(v, a, b)
  return math.min(math.max(v, a), b)
end

-------------------------------------------------- string extensions ---------------------------------------------------

function string.fill(str, length, character)
	character = character or " "
	if #str < length then
		n = (length - #str) / 2.0
		return string.rep(character, math.floor(n)) .. str .. string.rep(character, math.ceil(n))
	elseif #str > length then
		return str:sub(1, length)
	else
		return str
	end
end