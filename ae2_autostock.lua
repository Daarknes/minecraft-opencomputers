local component = require("component")
require("daarknes.core.luaextensions")
require("daarknes.core.colorprint")


-------------- configurable item stock ---------------
local items = {
	{name="appliedenergistics2:material", damage=22, amount=512}, -- logic processor
	{name="appliedenergistics2:material", damage=23, amount=512}, -- calculation processor
	{name="appliedenergistics2:material", damage=24, amount=512}, -- engineering processor
	{name="appliedenergistics2:material", damage=10, amount=512}, -- pure certus quartz crystal
	{name="appliedenergistics2:material", damage=12, amount=512}  -- pure fluix crystal
}

-- seconds between checks
loopDelay = 10
------------------------------------------------------


local function getAEInterface()
	local comp = component.list("me_")
	for address, cType in pairs(comp) do
		return component.proxy(address)
	end
	error("No AppliedEnergistics component found")
end

local ae = getAEInterface()

local function initialize()
	for _, stack, rm in ripairs(items) do
		craftables = ae.getCraftables({name=stack.name, damage=stack.damage})
		if craftables.n > 0 then
			stack.craftable = craftables[1]
		else
			colorprint(colored("Missing pattern for ", 0xFF0000), colored(stack.name, 0xCCCC00), colored(" -> Removed from list.", 0xFF0000))
			rm()
		end
	end
end

local function autostock()
	print("Starting check ...")
	tasks = {}

	for _, stack in ipairs(items) do
		local aeStack = ae.getItemsInNetwork({name=stack.name, damage=stack.damage})
		local toCraft = stack.amount

		-- this should never be false but you never know
		if aeStack.n > 0 then
			toCraft = toCraft - aeStack[1].size
		end

		if toCraft > 0 then
			task = stack.craftable.request(toCraft)
			table.append(tasks, task)

			text = {"Requesting ", colored(toCraft, 0x00FFFF), " of ", colored(aeStack[1].label, 0x44FF44),
				" (", colored(stack.name, 0xCCCC00)}
			if stack.damage then
				table.append(text, colored(" #" .. stack.damage, 0xCCCC00))
			end
			table.append(text, ")")
			colorprint(table.unpack(text))
		end
	end
	
	while true do
		finished = true
		for _, task, rm in ripairs(tasks) do
			if task.isDone() or task.isCanceled() then
				rm()
			else
				finished = false
				break
			end
		end
		if finished then
			break
		end
		os.sleep(2)
	end
	
	print("Finished crafting.")
end


initialize()
if table.isEmpty(items) then
	print("there is nothing to keep stocked, stopping...")
	os.exit()
end

while true do
	autostock()
	os.sleep(loopDelay)
end