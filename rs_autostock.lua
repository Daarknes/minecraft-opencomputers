local component = require("component")
require("daarknes.core.luaextensions")
require("daarknes.core.colorprint")


-------------- configurable item stock ---------------
local items = {
	{name = "refinedstorage:processor", damage=3, amount=512},
	{name = "refinedstorage:processor", damage=5, amount=512},
	{name = "refinedstorage:processor", damage=4, amount=512}
}

-- seconds between checks
loopDelay = 10
------------------------------------------------------


local function getRSInterface()
	local comp = component.list("refinedstorage")
	for address, cType in pairs(comp) do
		return component.proxy(address)
	end
	error("No RefinedStorage component found")
end

local rs = getRSInterface()

local function initialize()
	for _, stack, rm in ripairs(items) do
		if not rs.hasPattern(stack) then
			colorprint(colored("Missing pattern for ", 0xFF0000), colored(stack.name, 0xCCCC00), colored(" -> Removed from list.", 0xFF0000))
			rm()
		end
	end
end

local function equals(stack1, stack2)
	local name = stack1.name == stack2.name
	local dmg = stack1.damage or 0 == stack2.damage or 0
	return name and dmg
end

local function autostock()
	print("Starting check ...")
	local tasks = rs.getTasks()

	for _, stack in ipairs(items) do
		local rsStack = rs.getItem(stack)
		local toCraft = stack.amount

		if rsStack ~= nil then
			toCraft = toCraft - rsStack.size
		end

		if toCraft > 0 then
			for _, task in ipairs(tasks) do
				if equals(stack, task.stack) then
					toCraft = toCraft - task.quantity
				end
			end
			if toCraft > 0 then
				rs.scheduleTask(stack, toCraft)

				local label = ".?."
				if rsStack ~= nil then
					label = rsStack.label
				end

				text = {"Requesting ", colored(toCraft, 0x00FFFF), " of ", colored(label, 0x44FF44),
				" (", colored(stack.name, 0xCCCC00)}
				if stack.damage then
					table.append(text, colored(" #" .. stack.damage, 0xCCCC00))
				end
				table.append(text, ")")
				colorprint(table.unpack(text))
			end
		end
	end
	print("All requests sent.")
end


initialize()
if table.isEmpty(items) then
	print("there is nothing to keep stocked, stopping...")
	os.exit()
end

while true do
	autostock()
	os.sleep(loopDelay)
end