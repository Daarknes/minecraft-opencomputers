local shell = require("shell")
local component = require("component")
local computer = require("computer")
local reactor = component.br_reactor
local utils = require("utils")

local args, ops = shell.parse(...)
if ops.help then
  print(
[[Usage: reactorControl [function]
available functions: heatUp, calculateStats, setRodLevel, getRodLevel, start]]
)
  return
end

-- available commands:
-- doEjectFuel function
-- doEjectWaste function
-- getActive function
-- getCasingTemperature function
-- getConnected function
-- getControlRodLevel function
-- getControlRodLocation function
-- getControlRodName function
-- getCoolantAmount function
-- getCoolantAmountMax function
-- getCoolantType function
-- getEnergyCapacity function
-- getEnergyProducedLastTick function
-- getEnergyStored function
-- getFuelAmount function
-- getFuelAmountMax function
-- getFuelConsumedLastTick function
-- getFuelReactivity function
-- getFuelTemperature function
-- getHotFluidAmount function
-- getHotFluidAmountMax function
-- getHotFluidProducedLastTick function
-- getHotFluidType function
-- getMaximumCoordinate function
-- getMinimumCoordinate function
-- getMultiblockAssembled function
-- getNumberOfControlRods function
-- getWasteAmount function
-- isActivelyCooled function
-- setActive function
-- setAllControlRodLevels function
-- setControlRodLevel function
-- setControlRodName function
-- slot -1
-- type br_reactor

local benchmarkDuration = 6
local statFile = "stats.txt"
local tolerance = 0.09


local maxEnergy = reactor.getEnergyCapacity()
local onoffConst = {
  lower = 0.09,
  upper = 0.9
}

local stats = {
  peakProductionRate = 0
}
local lastEnergyStored = reactor.getEnergyStored()
local energyExtractionRate = 0
local lastTicks = computer.uptime() * 20
local currentOutput = 0


local function loadStats(fileName)
	local f = io.open(fileName, "r")

	if f == nil then
		return false
	else
		stats.peakProductionRate = tonumber(f:read())
		print("loaded stats (" .. fileName .. "):", stats.peakProductionRate)
		f:close()
		return true
	end
end

local function saveStats(fileName)
	local f = io.open(fileName, "w")
	f:write(stats.peakProductionRate)
	f:close()
	print("saved stats (" .. fileName .. "):", stats.peakProductionRate)
end

function heatUp()
	local lastTemp = 0
	local currentTemp = reactor.getCasingTemperature()
	-- let the reactor heat up
	while currentTemp > lastTemp do
		lastTemp = currentTemp
		os.sleep(1)
		currentTemp = reactor.getCasingTemperature()
	end
end

function calculateStats()
	print("starting benchmark")
	reactor.setAllControlRodLevels(0)
	reactor.setActive(true)

  

	local start = computer.uptime()
	local energyMax = 0
	while computer.uptime() - start < benchmarkDuration do
		energyMax = math.max(energyMax, reactor.getEnergyProducedLastTick())
		os.sleep(0.5)
	end

	stats.peakProductionRate = energyMax
	print("finished benchmark.", "Peak energy production:", stats.peakProductionRate)
	reactor.setActive(false)
end

function setRodLevel(level)
	local n = reactor.getNumberOfControlRods()
	local rest = math.floor((level % 1.0) * n + 0.5) - 1
	level = math.floor(level)
	
	for i=0, n-1 do
		if i <= rest then
			reactor.setControlRodLevel(i, level + 1)
		else
			reactor.setControlRodLevel(i, level)
		end
	end
end

function getRodLevel()
	local n = reactor.getNumberOfControlRods()
	local sum = 0

	for i=0, n-1 do
		sum = sum + reactor.getControlRodLevel(i)
	end

	return sum / n
end

local function calibrate()
	local currentEnergyStored = reactor.getEnergyStored()
	local energyDiff = currentEnergyStored - lastEnergyStored
	local curTicks = computer.uptime() * 20
	local tickDiff = curTicks - lastTicks
  
	local productionRate = reactor.getEnergyProducedLastTick()
	energyExtractionRate = productionRate - energyDiff / tickDiff

	local lastRodLevel = getRodLevel()
	local rodLevel = lastRodLevel
	-- more energy produced than consumed
	if productionRate > energyExtractionRate + tolerance * productionRate then
		local factor = (productionRate - energyExtractionRate) / stats.peakProductionRate
		rodLevel = rodLevel + math.max((100 - rodLevel) * factor / 2, 1)
	-- more energy consumed than produced
	elseif productionRate <= energyExtractionRate and rodLevel > 0 then
		local factor = (energyExtractionRate - productionRate) / stats.peakProductionRate
		rodLevel = rodLevel - math.max(rodLevel * factor / 2, 1)
	end

	rodLevel = utils.clamp(rodLevel, 0, 99)
	if (math.abs(rodLevel - lastRodLevel) >= 0.5) then
		msg = string.format("extracted: %.0f RF/t. produced: %.0f RF/t. Rod Level: %.2f", energyExtractionRate, productionRate, rodLevel)
		print("Rod level change:", msg)
	end
	setRodLevel(rodLevel)
	-- sleep a few moments to let the reactor adjust
	local sleepDuration = math.pow(math.abs(lastRodLevel - rodLevel), 0.7) * 2
	os.sleep(sleepDuration)

	lastTicks = computer.uptime() * 20
	lastEnergyStored = reactor.getEnergyStored()
end


local function offOnMode()
	energy = reactor.getEnergyStored()
	if energy > onoffConst["upper"] * maxEnergy then
		reactor.setActive(false)
	elseif energy < onoffConst["lower"] * maxEnergy then
		reactor.setActive(true)
		-- wait for the reactor to reach max temp after turning it on
		heatUp()
	end
end



local running = true

function start()
	if not loadStats(statFile) then
		calculateStats()
		saveStats(statFile)
	end

	while running do
		offOnMode()
		if reactor.getActive() then
			calibrate()
		end
		os.sleep(1)
	end
end

function stop()
	running = false
end

if #args > 0 then
	local f = _G[args[1]]
	if f then
		table.remove(args, 1) 
		print(f(table.unpack(args)))
	else
		print(string.format("couldn't find function %q", args[1]))
	end
else
	start()
end